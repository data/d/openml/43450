# OpenML dataset: Milan-Airbnb-Open-Data-(only-entire-apartments)

https://www.openml.org/d/43450

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

Introduction
The dataset contains all the entire apartments located in Milan (N = 9322). 
This public dataset is part of Airbnb, and the original source can be found on this website.
Dataset Creation
From the original dataset:
1) Nuisance variables were removed. 
2) Variables were recoded in order to be clear and intuitive. 
3) A series of dummy variables were created based on the services offered by each apartment (TV, WiFi, AirCondition, Wheelchairaccessible, Kitchen, Breakfast, Elevator, Heating, Washer, Iron, Hostgreetsyou, Paidparkingonpremises, Luggagedropoffallowed, Longtermstaysallowed, Doorman, Petsallowed, Smokingallowed, Suitableforevents, 24hourcheck_in).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/43450) of an [OpenML dataset](https://www.openml.org/d/43450). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/43450/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/43450/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/43450/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

